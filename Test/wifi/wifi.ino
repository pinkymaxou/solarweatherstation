
#include <driver/adc.h>
#include <esp_adc_cal.h>
#include <esp_pm.h>
#include <esp_bt.h>
#include <esp_sleep.h>
#include <esp_wifi.h>
#include <rom/rtc.h>
#include "soc/soc.h"
#include "soc/rtc_cntl_reg.h"

#include <WiFi.h>
#include <HTTPClient.h>

// Wifi
const char* WIFI_SSID = "Maxou_TestIOT";
const char* WIFI_PASSWORD = "!UnitedStatesOfSmash97!";
const int   WIFI_CONNECTIONTIMEOUTMS = 8000; // in milliseconds

// HTTP
const int   HTTP_REQUEST_TIMEOUT_MS = 50*1000;

const char* HTTP_UPDATE_URL = "http://192.168.5.5/Main/PostData"; // 
const char* HTTP_UPDATE_ENDPOINT = "/Main/PostData";
const int   HTTP_UPDATE_PORT = 51248;

void setup() {
  // put your setup code here, to run once:

  // WIFI PART ....
  // put your setup code here, to run once:
  WiFi.disconnect(true);
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
  // WiFi.setSleep(true);

  Serial.begin(115200);
  Serial.println("Device init...");
}

void loop() {
  Serial.println("WIFI connecting ...");

  long startConnMS = millis();
  
  while(WiFi.status() != WL_CONNECTED && (millis()-startConnMS) < WIFI_CONNECTIONTIMEOUTMS) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("WIFI Connected");
  
  do
  {
    Serial.println("HTTP: POST...");
    
    HTTPClient http;
    char payload[1024];

    // , HTTP_UPDATE_PORT, HTTP_UPDATE_ENDPOINT
    http.begin(HTTP_UPDATE_URL);
    
    http.setTimeout(HTTP_REQUEST_TIMEOUT_MS);
    http.addHeader("Content-Type", "application/x-www-form-urlencoded");
  
    sprintf(payload, "s=%s&battv=%f&wc=%d&boxtemp=%.2f&boxhumidity=%.2f&light=%.2f&powersave=%d&exttempc=%.2f&pressurepa=%.2f&m=coucoualex", 
        "TEST2", 4.12, 10,0, 0, 0, 1, 0, 0 );
    
    Serial.print("payload: ");
    Serial.println(payload);
    
    int httpCode = http.POST(payload);

    Serial.print("http code: ");
    Serial.print(httpCode);
    Serial.println("");
    http.end();

    Serial.println("done");
    Serial.flush();
    // Ask wifi to operate on next boot ...
    esp_sleep_enable_timer_wakeup(250); // Basically we just want to reset wihtout destroying RTC RAM content...
    esp_deep_sleep_start(); // Good night
  }
  while(true);
  
}
